let logInForm = document.querySelector('#logInUser');

let main = document.querySelector('main');

main.classList.add("d-block");

logInForm.addEventListener('submit', (e) => {
	e.preventDefault()
	let email = document.querySelector('#userEmail').value;
	let password = document.querySelector('#password').value;
	console.log(email, password)
	// fetch
	fetch(`${url}/api/users/login`, {
		method	: "POST",
		body	: JSON.stringify({
			email		: email,
			password	: password
		}),
		headers: {
			'Content-Type' : 'application/json'
		}
	})
	.then( res => res.json())
	.then( data => {
		if(data.accessToken) {
			localStorage.setItem('token', `Bearer ${data.accessToken}`)
			fetch(`${url}/api/users/details`, {
				headers: {
					Authorization: localStorage.getItem('token')
				}
			})
			.then( res => res.json())
			.then( data => {
				console.log(data)
				localStorage.setItem("id", data._id)
				localStorage.setItem("isAdmin", data.isAdmin)
				window.location.replace("./courses.html")
			})
		} else {
			alert("Something went wrong!!!")
		}
	})
})