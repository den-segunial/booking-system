let token = localStorage.getItem('token');
let zuitterNav = document.querySelector('#zuitterNav');
let pages = window.location.pathname.split('/');
let checkHome = (pages.findIndex( (curr, index) => { return curr === 'pages' }) === -1) ? './pages/' : './'
let currentPage = pages[pages.length-1];

// console.log('token', token)
// console.log('currentPage', currentPage)
// console.log('checkHome', checkHome)

let loginBtn = (checkToken) => {
	let active = (currentPage === 'login.html') ? 'active' : '' 
	if(checkToken === null) {
		return `
			<li class="nav-item ">
				<a href="${checkHome}login.html" class="nav-link ${active}"> Login </a>
			</li>
		`
	} else {
		return `
			<li class="nav-item ">
				<a href="${checkHome}logout.html" class="nav-link ${active}"> Logout </a>
			</li>
		`
	}
}

let profileBtn = (checkToken) => {
	let active = () => {
		if(currentPage === 'profile.html' || currentPage === 'register.html') {
			return 'active'
		}
		return ''
	}
	if(checkToken === null) {
		return `
			<li class="nav-item ">
				<a href="${checkHome}register.html" class="nav-link ${active()}"> Register </a>
			</li>
		`
	} else {
		return `
			<li class="nav-item ">
				<a href="${checkHome}profile.html" class="nav-link ${active()}"> Profile </a>
			</li>
		`
	}
}

let headerNav = (headerData) => {
	// console.log('headerData', headerData)
	// Home Page
	if(currentPage === 'index.html') {
		return ` 
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active">
					<a href="./index.html" class="nav-link"> Home </a>
				</li>

				<li class="nav-item ">
					<a href="./pages/courses.html" class="nav-link"> Courses </a>
				</li>
				${profileBtn(token)}
				${loginBtn(token)}
			</ul>
		`
	} else if(currentPage === 'login.html') {
		return ` 
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a href="./../index.html" class="nav-link"> Home </a>
				</li>

				<li class="nav-item ">
					<a href="./pages/courses.html" class="nav-link"> Courses </a>
				</li>
				${profileBtn(token)}
				${loginBtn(token)}
			</ul>
		`
	} else if(currentPage === 'courses.html') {
		return ` 
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a href="./../index.html" class="nav-link"> Home </a>
				</li>

				<li class="nav-item ">
					<a href="./pages/courses.html" class="nav-link active"> Courses </a>
				</li>
				${profileBtn(token)}
				${loginBtn(token)}
			</ul>
		`
	} else {
		console.log('Register Profile Login Logout')
		return ` 
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a href="./../index.html" class="nav-link"> Home </a>
				</li>

				<li class="nav-item ">
					<a href="./courses.html" class="nav-link"> Courses </a>
				</li>
				${profileBtn(token)}
				${loginBtn(token)}
			</ul>
		`
	}
}

zuitterNav.innerHTML = headerNav(token);