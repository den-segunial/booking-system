let adminUser = localStorage.getItem("isAdmin");
let main = document.querySelector('main');

main.classList.add("d-block");
// fetch
fetch(`${url}/api/courses`, {
	method: 'GET',
	headers: {
		'Content-Type' : 'application/json',
		'Authorization': localStorage.getItem('token')
	}
})
.then( res => res.json ())
.then( data => {
	console.log(data) 
	let coursesContainer = document.querySelector('#coursesContainer');
	let addButton = document.querySelector('#addButton');
	let activateButton = (course) => {
		if(!course.isActive) {
			return `
				<a href="./activateCourse.html?courseId=${course._id}" class="btn btn-success text-white btn-block dangerButton">Activate Course</a>
			`;
		} else {
			return `
				<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block dangerButton">Disable Course</a>
			`;
		}
	}
	let cardFooter = (course) => {
		if (adminUser === "false" || !adminUser) {
			return `
				<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Select Course</a>
			`
		} else {
			return `
				<a href="./editCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block editButton" >Edit</a>
				${activateButton(course)}
				<a href="./course.html?courseId=${course._id}" class="btn btn-info text-white btn-block infoButton">Enrollees</a>
			`
		}
	}
	let courseCard = (course) => (`
			<div class="col-md-6 my-3">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">${course.name}</h5>
						<p class="card-text text-left">${course.description}</p>
						<div class="card-text text-right">&#8369; ${course.price}</div>
						<div class="card-footer">
							${cardFooter(course)}
						</div>
					</div>
				</div>
			</div> 
		`)
	let courseData 
	if(data.length < 1) {
		courseData = `<h5 class="d-block text-center">No courses available</h5>`;
	} else {
		courseData = data.results.map ( course => courseCard(course)).join("")
	}
	if(adminUser === "true") {
		addButton.innerHTML = `<a href="./addCourse.html" class="btn btn-outline-success">Add Course</a>`
	} else {
		addButton.innerHTML = `<a href="./courses.html" class="btn btn-outline-primary">Book Now!</a>`
	}
	coursesContainer.innerHTML = courseData;
})