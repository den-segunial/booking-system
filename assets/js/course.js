// console.log(window.location.search)

let params = new URLSearchParams(window.location.search);
let adminUser = localStorage.getItem("isAdmin");
let id = localStorage.getItem('id') 
// console.log(id)
// console.log(params)
// console.log(params.get('courseId'))

let courseId = params.get('courseId');
if(courseId === null) {
	window.location.replace('./courses.html')
}

let courseName = document.querySelector('#courseName');
let courseDesc = document.querySelector('#courseDesc');
let coursePrice = document.querySelector('#coursePrice');
let enrollContainer = document.querySelector('#enrollContainer');
let main = document.querySelector('main');

main.classList.add("d-block");
fetch(`${url}/api/courses/${courseId}`)
.then( res => res.json() )
.then( data => {
	// console.log(adminUser)
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price
	if(adminUser === "false" || !adminUser) {
		// Normal User
		enrollContainer.innerHTML = `<button class="btn btn-block btn-primary">Enroll</button>`	
		// console.log(data)
		let enrolleeChecker = data.enrollees.some( e => e.userId === id)
		// console.log(enrolleeChecker)
		let enrollButton = document.querySelector('#enrollContainer > button') 
		enrollButton.addEventListener('click', () => {
			if(!enrolleeChecker) {
				fetch(`${url}/api/users/enroll`, {
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json',
						'Authorization' : localStorage.getItem('token')
					},
					body: JSON.stringify({courseId})
				})
				.then( res => res.json() )
				.then( data => {
					// console.log(data)
					if(data) {
						if(id !== null) {
							alert('Thank you for enrolling! See you!')
							window.location.replace('./courses.html')
						} else {
							alert('Kindly register first to enroll')
							window.location.replace('./register.html')
						}
					} else {
						alert('Something went wrong!!')
					}
				})
			} else {
				alert('You have already enrolled on this course')
				window.location.replace('./courses.html')
			}
		})
	} else {
		fetch(`${url}/api/users/${courseId}`, {
			method: 'GET',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : localStorage.getItem('token')
			}
		})
		.then( res => res.json() )
		.then( data => {
			let enrollData = (data) => {
				if(data.length !== 0) {
					return data.map( (curr, index) => {
						return `<li class="text-primary">${curr.firstName} ${curr.lastName}</li>`
					}).join("")
				}
				return `<li>No Enrollees on this Course</li>`
			}
			let enrolleesTemplate = (data) => (`
				<ul class="text-left text-capitalize" aria-label="List of Enrollees:">
					${enrollData(data)}
				</ul>
			`);
			enrollContainer.innerHTML = enrolleesTemplate(data)
		})
	}
})