// console.log(window.location.search)

let params = new URLSearchParams(window.location.search);

// console.log(params)
// console.log(params.get('courseId'))

let courseId = params.get('courseId');


fetch(`${url}/api/courses/${courseId}`, {
	method: 'DELETE',
	headers: {
		'Authorization' : localStorage.getItem('token')
	}
})
.then( res => res.json() )
.then( data => {
	if(data) {
		window.location.replace('./courses.html')
	} else {
		alert('Something went wrong')
	}
})