let createCourse = document.querySelector('#createCourse');
let main = document.querySelector('main');

main.classList.add("d-block");
createCourse.addEventListener('submit', e => {
	e.preventDefault();

	// get all field values
	let courseName = document.querySelector('#courseName').value
	let coursePrice = document.querySelector('#coursePrice').value
	let courseDescription = document.querySelector('#courseDescription').value

	let isFormComplete = 
	courseName !== "" &&
	coursePrice !== "" &&
	courseDescription !== "";
	// send the data to the backend
	if(isFormComplete) {
		fetch(`${url}/api/courses`, {
			method: 'POST',
			headers: {
				'Content-Type' 	: 'application/json',
				'Authorization' : localStorage.getItem('token')
			},
			body: JSON.stringify({
				name: courseName,
				description: courseDescription,
				price: coursePrice
			})
		})
		.then( res => res.json() )
		.then( data => {
			// console.log(data)
			if(data) {
				window.location.replace('./courses.html')
			} else {
				alert('Something went wrong!!!')
			}
		})
	} else {
		alert("Need to input all data")
	}

	// if the fetch result is true
	// redirect use to the courses.html
	// else alert
})