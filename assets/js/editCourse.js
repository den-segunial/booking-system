// console.log(window.location.search)

let params = new URLSearchParams(window.location.search);

// console.log(params)
// console.log(params.get('courseId'))

let courseId = params.get('courseId');

console.log()
if(courseId === null) {
	window.location.replace('./courses.html')
}

let name = document.querySelector('#courseName');
let price = document.querySelector('#coursePrice');
let description = document.querySelector('#courseDescription');
let editCourse = document.querySelector('#editCourse');
let main = document.querySelector('main');

main.classList.add("d-block");

console.log(main)

fetch(`${url}/api/courses/${courseId}`)
.then( res => res.json() )
.then( data => {
	// console.log(data)

	if(data) {

		name.value = data.name;
		price.value = data.price;
		description.value = data.description;
	} else {
		alert("Something went wrong")
	}

})

editCourse.addEventListener('submit', (e) => {
	e.preventDefault();

	let nameVal = name.value;
	let priceVal = price.value;
	let descriptionVal = description.value;

	fetch(`${url}/api/courses`, {
		method: 'PUT',
		headers: {
			'Content-Type' 	: 'application/json',
			'Authorization' : localStorage.getItem('token')
		},
		body: JSON.stringify({
    		courseId,
    		name: nameVal,
    		description: descriptionVal,
    		price: priceVal
		})	
	})
	.then( res => res.json() )
	.then( data => {
		if(data === true) {
			window.location.replace('./courses.html')
		} else {
			alert('Something went wrong')
		}
	})
})